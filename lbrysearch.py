# -*- coding: utf-8 -*-
"""
lbrysearch.py - Sopel lbry/odysee search
Copyright © 2021, Duck Hunt-Pr0, irc.stembod.org
Licensed under the GNU GENERAL PUBLIC LICENSE, Version 3

This Sopel module searches LBRY for stuff via lbrynet api daemon.
"""


LBRY_API_SERVER = "http://localhost:20123" #default lbrynet API host+port to use


import requests
#from sopel import module, plugin




def isLBRYnetRunning():
    """simply check if lbrynet is available at LBRY_API_SERVER

       return True if it is, False if not""" 

    QUERY = {"method": "version", "params": {}}     # https://lbry.tech/api/sdk#version
    
    try:
        RESULTS= requests.post(LBRY_API_SERVER, json=QUERY).json()
    except requests.exceptions.ConnectionError:
        return False

    if RESULTS['jsonrpc']:
        return True
    

def search_lbry(text_to_search=False, channel_to_search=False, tags_to_search=False):
    """Search lbrynet by utilizing the json API ( https://lbry.tech/api/sdk#claim_search )"""

    ### DO ERROR CHECKING HERE(?) ##
    # check parameter formats given first?

    ### DO CONNECTION CHECK ###
    # No point in continuing if lbrynet is not there..
    if isLbryNetRunning() == False: return False

    ### DEFAULT JSON QUERY ###
    QUERY = {"method": "claim_search",
            "params": { "claim_ids": [],
                "channel": "@lbry",
                "channel_ids": [],
                "not_channel_ids": [],
                "has_channel_signature": False,
                "valid_channel_signature": False,
                "invalid_channel_signature": False,
                "is_controlling": False,
                "stream_types": [],
                "media_types": [],
                "any_tags": [],
                "all_tags": [],
                "not_tags": [],
                "any_languages": [],
                "all_languages": [],
                "not_languages": [],
                "any_locations": [],
                "all_locations": [],
                "not_locations": [],
                "order_by": [],
                "no_totals": True,
                "include_purchase_receipt": False,
                "include_is_my_output": False,
                "remove_duplicates": True,
                "has_source": False,
                "has_no_source": False,
                "text": "lbry",
                "tags": ["lbry","odysee"]}}

    # remove 'channel' if not channel specified
    if not channel_to_search:
        QUERY["params"].pop("channel")
    # remove 'text' if no text search specififed
    if not text_to_search:
        QUERY["params"].pop("text")
    # remove 'tags' if none specified
    if not tags_to_search:
        QUERY["params"].pop("tags")

    RESULTS= requests.post(LBRY_API_SERVER, json=QUERY).json()

    print(RESULTS)

#@module.commands('lbry')
#def helloworld(bot, trigger):
#    joined_nick = str(trigger.nick)
#    bot.say( "lbry plugin test" )

#search_lbry(text_to_search="odysee")

print( isLBRYnetRunning() )



############################ NOTES ############################ 
#
# curl -d'{"method": "claim_search", "params": {"claim_ids": [], "channel_ids": [], "not_channel_ids": [], "has_channel_signature": false, "valid_channel_signature": false, "invalid_channel_signature": false, "is_controlling": false, "stream_types": [], "media_types": [], "any_tags": [], "all_tags": [], "not_tags": [], "any_languages": [], "all_languages": [], "not_languages": [], "any_locations": [], "all_locations": [], "not_locations": [], "order_by": [], "no_totals": false, "include_purchase_receipt": false, "include_is_my_output": false, "remove_duplicates": false, "has_source": false, "has_no_source": false, "tags" : ["technology"], "text": "fluke atombomb"}}' http://localhost:20123

